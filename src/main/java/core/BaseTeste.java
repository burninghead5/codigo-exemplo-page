package core;

import org.junit.After;
import org.junit.Before;

import pages.LoginPage;

public class BaseTeste {
	
	private LoginPage page = new LoginPage();
	
	@Before
	public void inicializa(){
		page.acessarUrl();
	}
	
	@After
	public void finalizar(){
		if(Propriedades.FECHAR_BROWSER){
			DriverFactory.killDriver();
		}
	}

}
