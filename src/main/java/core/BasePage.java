package core;

import org.openqa.selenium.By;

public class BasePage {
	
	public void escrever(By by, String texto){
		DriverFactory.getDriver().findElement(by).clear();
		DriverFactory.getDriver().findElement(by).sendKeys(texto);
	}
	
	public void escrever (String id_campo, String texto){
		escrever(By.id(id_campo), texto);
	}
	
	public void escreverPorXpath(String xpath, String texto){
		escrever(By.xpath(xpath), texto);
	}
	
	public void clicarBotao(String xpath){
		DriverFactory.getDriver().findElement((By.xpath(xpath))).click();
	}
}
