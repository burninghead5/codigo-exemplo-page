package pages;

import org.openqa.selenium.By;

import core.BasePage;
import core.DriverFactory;

public class LoginPage extends BasePage {
	
	public void acessarUrl(){
		DriverFactory.getDriver().get("http://a.testaddressbook.com/sign_in");
	}
	
	public void setEmail(String texto){
		escrever((By.id("session_email")), texto);
	}
	
	public void setSenha(String texto){
		escrever((By.id("session_password")), texto);
	}
	
	public void signIn(){
		clicarBotao("//*[@value='Sign in']");
		
	}
	
	

}
